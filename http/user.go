package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/g.e.mcdaniel/crudl"
	"gitlab.com/g.e.mcdaniel/crudl/internal/id"
)

// registerUserRoutes sets up the routes for the CRUDL operations on users
func (s *Server) registerUserRoutes(r *mux.Router) {
	r.HandleFunc("/users", s.handleListUsers).Methods(http.MethodGet)
	r.HandleFunc("/users", s.handleCreateUser).Methods(http.MethodPost)
	r.HandleFunc("/users/{id}", s.handleGetUser).Methods(http.MethodGet)
	r.HandleFunc("/users/{id}", s.handleUpdateUser).Methods(http.MethodPut)
	r.HandleFunc("/users/{id}", s.handleDeleteUser).Methods(http.MethodDelete)
}

// handleListUsers is the handler function for listing users for the service.
// TODO implement page, per, sort, and order
func (s *Server) handleListUsers(w http.ResponseWriter, r *http.Request) {
	var (
		page        int = 1
		per         int = 10
		sort, order string
		err         error
	)

	q := r.URL.Query()

	if p := q.Get("page"); p != "" {
		page, err = strconv.Atoi(p)
		if err != nil || 0 >= page {
			handleError(w, http.StatusBadRequest, fmt.Errorf("invalid value for parameter 'page': '%s' should be an integer greater than 0", p))
			return
		}
	}

	if p := q.Get("per"); p != "" {
		per, err = strconv.Atoi(p)
		if err != nil || 0 >= per {
			handleError(w, http.StatusBadRequest, fmt.Errorf("invalid value for parameter 'per': '%s' should be an integer greater than 0", p))
			return
		}
	}

	sort = q.Get("sort")
	order = q.Get("order")

	users, err := s.UserService.Users(page, per, sort, order)
	if err != nil {
		handleError(w, http.StatusInternalServerError, fmt.Errorf("failed to list users: %v", err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(users); err != nil {
		fmt.Fprintf(os.Stderr, "failed to write list users response: %v", err)
		return
	}
}

// handleCreateUser is the handler function for creating a user.
func (s *Server) handleCreateUser(w http.ResponseWriter, r *http.Request) {
	var user *crudl.User

	defer r.Body.Close()
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		handleError(w, http.StatusBadRequest, fmt.Errorf("failed to parse body of request: %v", err))
		return
	}

	// Check user provided input has required fields
	if err := user.Validate(); err != nil {
		handleError(w, http.StatusBadRequest, err)
		return
	}

	// Set ID, create timestamp, and clear out UpdatedAt (if user passed it in)
	user.ID = id.GetNextID()
	user.CreateAt = time.Now()
	user.UpdatedAt = nil

	if err := s.UserService.CreateUser(user); err != nil {
		handleError(w, http.StatusInternalServerError, fmt.Errorf("failed to create user: %v", err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(user); err != nil {
		fmt.Fprintf(os.Stderr, "failed to write list users response: %v", err)
		return
	}
}

// handleGetUser is the handler function for retrieving a specific user.
func (s *Server) handleGetUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		handleError(w, http.StatusBadRequest, fmt.Errorf("invalid value for ID: expected integer"))
		return
	}

	user, err := s.UserService.User(id)
	if err != nil {
		handleError(w, http.StatusNotFound, fmt.Errorf("user with id '%d' not found", id))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(user); err != nil {
		fmt.Fprintf(os.Stderr, "failed to write get user response: %v", err)
		return
	}
}

// handleUpdateUser is the handler function for updating a users attributes.
// Because we are using a k-v store for our database, we need to retrieve the user
// first to copy over the create_at field. We also validate the user input since PUT
// is a replacement operation and not PATCH.
func (s *Server) handleUpdateUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		handleError(w, http.StatusBadRequest, fmt.Errorf("invalid value for ID: expected integer"))
		return
	}

	// retrieve the existing user to verify they exist, and for copying the create_at field
	existingUser, err := s.UserService.User(id)
	if err != nil {
		handleError(w, http.StatusNotFound, fmt.Errorf("user with id '%d' not found", id))
		return
	}

	var user *crudl.User

	defer r.Body.Close()
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		handleError(w, http.StatusBadRequest, fmt.Errorf("failed to parse body of request: %v", err))
		return
	}

	// Check user provided input has required fields
	if err := user.Validate(); err != nil {
		handleError(w, http.StatusBadRequest, err)
		return
	}

	now := time.Now()

	// Set ID, create timestamp, and UpdatedAt
	user.ID = id
	user.CreateAt = existingUser.CreateAt
	user.UpdatedAt = &now

	if err := s.UserService.UpdateUser(user); err != nil {
		handleError(w, http.StatusInternalServerError, fmt.Errorf("failed to update user with '%d': %v", id, err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(user); err != nil {
		fmt.Fprintf(os.Stderr, "failed to write list users response: %v", err)
		return
	}
}

// handleDeleteUser is the handler function for deleting a specific user.
func (s *Server) handleDeleteUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		handleError(w, http.StatusBadRequest, fmt.Errorf("invalid value for ID: expected integer"))
		return
	}

	if _, err := s.UserService.User(id); err != nil {
		handleError(w, http.StatusNotFound, fmt.Errorf("user with id '%d' not found", id))
		return
	}

	if err := s.UserService.DeleteUser(id); err != nil {
		handleError(w, http.StatusInternalServerError, fmt.Errorf("failed to delete user with '%d': %v", id, err))
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
