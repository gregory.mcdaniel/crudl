package http

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"gitlab.com/g.e.mcdaniel/crudl"
)

// Server represents the HTTP Server serving up the user API.
type Server struct {
	server *http.Server
	router *mux.Router

	UserService crudl.UserService
}

// NewServer creates a new instances of the server
func NewServer() *Server {
	s := &Server{
		server: &http.Server{
			Addr: ":8080",
		},
		router: mux.NewRouter(),
	}

	// using negroni for some default middlewares
	n := negroni.Classic()
	n.UseHandler(s.router)

	s.server.Handler = n

	// register the user routes for this app
	s.registerUserRoutes(s.router)

	return s
}

// Open begins listening on port 8080 for the API
func (s *Server) Open() error {
	go s.server.ListenAndServe()

	return nil
}

// Close shuts the server down
func (s *Server) Close() error {
	return s.server.Shutdown(context.Background())
}
