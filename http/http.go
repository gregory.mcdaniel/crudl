package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

// Error is the error message returned to the user
type Error struct {
	Error string `json:"error"`
}

// handleError is a helper to write the error back to the user
func handleError(w http.ResponseWriter, status int, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if hErr := json.NewEncoder(w).Encode(Error{Error: err.Error()}); hErr != nil {
		fmt.Fprintf(os.Stderr, "failed to write error response: %v", hErr)
	}
}
