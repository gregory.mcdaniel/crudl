package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/dgraph-io/badger/v3"
	"gitlab.com/g.e.mcdaniel/crudl"
	"gitlab.com/g.e.mcdaniel/crudl/database"
	"gitlab.com/g.e.mcdaniel/crudl/http"
	"gitlab.com/g.e.mcdaniel/crudl/internal/id"
)

func main() {
	// Setup signal handling for graceful shutdown of the server
	ctx, cancel := context.WithCancel(context.Background())
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	go func() {
		<-quit
		cancel()
	}()

	a := New()

	// kickoff the application to start the server
	if err := a.Run(); err != nil {
		a.Close()
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	// wait for the shutdown signal
	<-ctx.Done()

	if err := a.Close(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// App is the main app.
type App struct {
	HTTPServer *http.Server
	DB         *badger.DB
}

// New create a new instances of the App configured with a database and HTTP server.
func New() *App {
	db, err := badger.Open(badger.DefaultOptions("").WithInMemory(true))
	if err != nil {
		panic(err)
	}

	return &App{
		HTTPServer: http.NewServer(),
		DB:         db,
	}
}

// Close gracefully closes the app
func (a *App) Close() error {
	if a.HTTPServer != nil {
		if err := a.HTTPServer.Close(); err != nil {
			return err
		}
	}

	if a.DB != nil {
		if err := a.DB.Close(); err != nil {
			return err
		}
	}
	return nil
}

// Run executes the app and starts the API.
func (a *App) Run() error {
	// Create the user service
	userService := database.NewUserService(a.DB)

	// Populate the database with some initial users
	if err := populateDB(userService); err != nil {
		return err
	}

	// Assign the user service to the app
	a.HTTPServer.UserService = userService

	if err := a.HTTPServer.Open(); err != nil {
		return err
	}

	return nil
}

// populateDB create 10 users to pre populate the database with for this challenge
func populateDB(s crudl.UserService) error {
	now := time.Now()
	for _, user := range []*crudl.User{
		{ID: id.GetNextID(), FirstName: "Gregory", LastName: "McDaniel", Email: "gregory.mcdaniel@crudl.api", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "John", LastName: "Doe", Email: "john.doe@example.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Jane", LastName: "Doe", Email: "jan.doe@example.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Tres", LastName: "Bailey", Email: "tres.bailey@changehealthcare.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "James", LastName: "Smith", Email: "james.smith@testing.api", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Maria", LastName: "Garcia", Email: "mgarcia@myapp.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Jennifer", LastName: "Williams", Email: "j.williams@common.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Elizabeth", LastName: "Davis", Email: "liz.davis@home.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Patricia", LastName: "Brown", Email: "pb@j.com", CreateAt: now},
		{ID: id.GetNextID(), FirstName: "Charles", LastName: "Lee", Email: "chuck.lee@enterprises.com", CreateAt: now},
	} {
		if err := s.CreateUser(user); err != nil {
			return err
		}
	}
	return nil
}
