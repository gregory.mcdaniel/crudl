module gitlab.com/g.e.mcdaniel/crudl

go 1.16

require (
	github.com/dgraph-io/badger/v3 v3.2011.1
	github.com/gorilla/mux v1.8.0
	github.com/urfave/negroni v1.0.0
)
