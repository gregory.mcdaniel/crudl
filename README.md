# CRUDL API

CRUDL is an simple API that performs basic CRUD+L operations on a User.

## Getting Started

### Executing the API
To start using CRUDL, install Go 1.16 or newer. CRUDL uses go modules. Run the following the root of the project to start the API.

```sh
$ go run cmd/crudld/main.go
```
This will start the API locally on port 8080. By default, the service will be loaded with 10 example users.

##### Note: CRUDL does not directly use CGO but it relies on [BadgerDB](https://github.com/dgraph-io/badger) for the underlying database and it relies on a library that uses gcc/cgo. If you wish to use CRUDL without gcc/cgo, you can run `CGO_ENABLED=0 go run cmd/crudld/main.go` which will run CRUDL without the support for ZSTD compression algorithm for badger.

### Calling the API
The following sections will show examples of each of the CRUD+L calls.

#### Create a User

To create a user, you will call the `/users` endpoint with the POST method providing a users object that defines the `first_name`, `last_name`, and `email` of the user. Failure to provide a non-empty value will result in an error. Extra fields are ignored.

Example call using cURL:

```sh
$ curl -k -XPOST http://localhost:8080/users -d'{"first_name":"Joe","last_name":"Biden","email":"president@whitehouse.com"}'
```

#### Retrieve a User

To retrieve a user, you will call the `/users/{id}` endpoint with the GET method where `{id}` is the ID of the user that is to be retrieved.

Example call using cURL:

```sh
$ curl -k -XGET http://localhost:8080/users/10
```

#### Update a User

To update a user, you will call the `/users/{id}` endpoint with the PUT method where `{id}` is the ID of the user that is to be updated. All fields need to be specified since PUT (update) replaces the user (instead of modifying done with PATCH which isn't implemented).

Example call using cURL:

```sh
$ curl -k -XPUT http://localhost:8080/users/10 -d'{"first_name":"Joseph","last_name":"Biden","email":"president@whitehouse.com"}'
```

#### Delete a User

To delete a user, you will call the `/users/{id}` endpoint with the DELETE method where `{id}` is the ID of the user that is to be deleted.

Example call using cURL:

```sh
$ curl -k -XDELETE http://localhost:8080/users/10
```

#### List Users

To list all users, you will need to call the `/users` endpoint using the GET method. By default, you will be provided a response of 10 users per page. The output will have the following schema:

```json
{
    "users": [
        <user>
    ],
    "count": <int>,
    "next_page": <int>
}
```

`next_page` will be omitted if there is no additional pages.

To customize the size of the page, you can provide the query parameter `per` with a greater than 0 integer.

To page through the results, you can specify the page with the query parameter `page` with a greater than 0 integer.

In addition to paging you can sort by any of the user fields by specifying a query parameter `sort` with a value of the field name to sort (`id`, `first_name`, `last_name`, `email`, `create_at`, and `updated_at`). Sorting order is defined by the query parameter `order` which can have a value of `asc` or `desc`. By default, users are sorted by `id` and order is defaulted to `asc`. If a value for `sort` or `order` is invalid, the default will be used.

Example call using cURL:

```sh
$ curl -k -XGET "http://localhost:8080/users?page=2&per=5&sort=last_name&order=desc"
```

## Time Spent

Time involved to create this was around 6-8 hours. Most of this was broken up in many short working sessions causing ramp up time to get back into where I had left off. In addition, I had to research a datastore to use and learn how to use it.

## Tradeoffs

There were various tradeoffs made during this project.

One of them was the datastore. In order to minimize learning and simplify code, I went with using BadgerDB for the datastore as opposed to using SQLite which would have been a nice solution in terms of user id generation, builtin sorting/ordering, etc with the downside of addition complication regarding library use, necessary error checking (injection), and the need to embed the schema.

Another was error handling, I could have defined my own error package for this example and used that to capture and report both codes and messages but it was a little heavy and time consuming such a simple implementation.

Another example was configuration. I decided to skip a CLI package with flags and/or a configuration file as it was overkill when I could just use simple defaults for this project.
