package database

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/dgraph-io/badger/v3"
	"gitlab.com/g.e.mcdaniel/crudl"
)

// UserService manages users.
type UserService struct {
	db *badger.DB
}

// Ensure the UserService implements the UserService interface
var _ crudl.UserService = (*UserService)(nil)

// NewUserService creates a new instance of the user service
func NewUserService(db *badger.DB) *UserService {
	return &UserService{
		db: db,
	}
}

// User retrieves a user by their id
func (s *UserService) User(id int) (*crudl.User, error) {
	user := new(crudl.User)
	if err := s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(strconv.Itoa(id)))
		if err != nil {
			return err
		}

		userBytes, err := item.ValueCopy(nil)
		if err != nil {
			return err
		}

		if err = json.Unmarshal(userBytes, &user); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}
	return user, nil
}

// Users retrieves all users with options to page, per, sort and order results
func (s *UserService) Users(page, per int, sort, order string) (*crudl.UsersPage, error) {
	users := make([]*crudl.User, 0, 10)
	if err := s.db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10

		it := txn.NewIterator(opts)

		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			userBytes, err := it.Item().ValueCopy(nil)
			if err != nil {
				return err
			}

			var user *crudl.User
			if err = json.Unmarshal(userBytes, &user); err != nil {
				return err
			}

			users = append(users, user)
		}

		return nil
	}); err != nil {
		return nil, err
	}

	// handle sorting and order first
	sortUsers(users, sort, order)

	totalUsers := len(users)
	offset := (page - 1) * per

	if offset >= totalUsers {
		return nil, fmt.Errorf("no additional pages")
	}

	usersPage := new(crudl.UsersPage)

	if (offset + per) < totalUsers {
		usersPage.Users = users[offset:(offset + per)]
	} else {
		usersPage.Users = users[offset:]
	}
	usersPage.Count = len(usersPage.Users)

	if (offset + usersPage.Count) < totalUsers {
		nextPage := page + 1
		usersPage.NextPage = &nextPage
	}

	return usersPage, nil
}

// CreateUser creates a user
func (s *UserService) CreateUser(user *crudl.User) error {
	if err := s.db.Update(func(txn *badger.Txn) error {
		userBytes, err := json.Marshal(user)
		if err != nil {
			return err
		}

		return txn.Set([]byte(strconv.Itoa(user.ID)), userBytes)
	}); err != nil {
		return err
	}
	return nil
}

// UpdateUser updates a user
func (s *UserService) UpdateUser(user *crudl.User) error {
	if err := s.db.Update(func(txn *badger.Txn) error {
		userBytes, err := json.Marshal(user)
		if err != nil {
			return err
		}

		return txn.Set([]byte(strconv.Itoa(user.ID)), userBytes)
	}); err != nil {
		return err
	}
	return nil
}

// DeleteUser deletes a user
func (s *UserService) DeleteUser(id int) error {
	if err := s.db.Update(func(txn *badger.Txn) error {
		return txn.Delete([]byte(strconv.Itoa(id)))
	}); err != nil {
		return err
	}
	return nil
}
