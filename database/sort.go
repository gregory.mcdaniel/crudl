package database

import (
	"strings"

	"gitlab.com/g.e.mcdaniel/crudl"
)

// sortUsers will sort and order a slice of crudl.User by attribute and order. Bad input is ignored.
func sortUsers(users []*crudl.User, sort, order string) {
	order = strings.ToLower(order)

	var sorter crudl.SortBy
	switch sort {
	case "first_name":
		sorter = crudl.ByFirstNameAsc
		if order == "desc" {
			sorter = crudl.ByFirstNameDesc
		}
	case "last_name":
		sorter = crudl.ByLastNameAsc
		if order == "desc" {
			sorter = crudl.ByLastNameDesc
		}
	case "email":
		sorter = crudl.ByEmailAsc
		if order == "desc" {
			sorter = crudl.ByEmailDesc
		}
	case "create_at":
		sorter = crudl.ByCreateAtAsc
		if order == "desc" {
			sorter = crudl.ByCreateAtDesc
		}
	case "updated_at":
		sorter = crudl.ByUpdatedAtAsc
		if order == "desc" {
			sorter = crudl.ByUpdatedAtDesc
		}
	default:
		sorter = crudl.ByIDAsc
		if order == "desc" {
			sorter = crudl.ByIDDesc
		}
	}

	sorter.Sort(users)
}
