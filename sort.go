package crudl

import "sort"

// userSorter joins a By function and a slice of User struct's to be sorted
type userSorter struct {
	users []*User
	by    func(u1, u2 *User) bool // Closure used in the Less method
}

// Implementation of the sort.Interface
func (u *userSorter) Len() int           { return len(u.users) }
func (u *userSorter) Swap(i, j int)      { u.users[i], u.users[j] = u.users[j], u.users[i] }
func (u *userSorter) Less(i, j int) bool { return u.by(u.users[i], u.users[j]) }

// SortBy allows one to define how to sort a slice of Users
type SortBy func(u1, u2 *User) bool

// Sort is a method on the function type, By, that sorts the argument slice according to the function
func (sb SortBy) Sort(users []*User) {
	us := &userSorter{
		users: users,
		by:    sb, // The Sort method's receiver is the function (closure) that defines the sort order
	}
	sort.Sort(us)
}

var (
	ByIDAsc        = SortBy(func(u1, u2 *User) bool { return u1.ID < u2.ID })
	ByFirstNameAsc = SortBy(func(u1, u2 *User) bool { return u1.FirstName < u2.FirstName })
	ByLastNameAsc  = SortBy(func(u1, u2 *User) bool { return u1.LastName < u2.LastName })
	ByEmailAsc     = SortBy(func(u1, u2 *User) bool { return u1.Email < u2.Email })
	ByCreateAtAsc  = SortBy(func(u1, u2 *User) bool { return u1.CreateAt.Before(u2.CreateAt) })
	ByUpdatedAtAsc = SortBy(func(u1, u2 *User) bool {
		if u2.UpdatedAt == nil {
			return false
		}

		if u1.UpdatedAt == nil {
			return true
		}

		return u1.UpdatedAt.Before(*u2.UpdatedAt)
	})

	ByIDDesc        = SortBy(func(u1, u2 *User) bool { return u1.ID > u2.ID })
	ByFirstNameDesc = SortBy(func(u1, u2 *User) bool { return u1.FirstName > u2.FirstName })
	ByLastNameDesc  = SortBy(func(u1, u2 *User) bool { return u1.LastName > u2.LastName })
	ByEmailDesc     = SortBy(func(u1, u2 *User) bool { return u1.Email > u2.Email })
	ByCreateAtDesc  = SortBy(func(u1, u2 *User) bool { return u1.CreateAt.After(u2.CreateAt) })
	ByUpdatedAtDesc = SortBy(func(u1, u2 *User) bool {
		if u1.UpdatedAt == nil {
			return false
		}

		if u2.UpdatedAt == nil {
			return true
		}

		return u1.UpdatedAt.After(*u2.UpdatedAt)
	})
)
