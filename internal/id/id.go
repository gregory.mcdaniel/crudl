package id

import "sync"

var (
	id   int
	idmu sync.Mutex
)

// GetNextID returns the next ID starting at 0.
func GetNextID() int {
	idmu.Lock()
	i := id
	id++
	idmu.Unlock()
	return i
}
