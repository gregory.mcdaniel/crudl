package crudl

import (
	"errors"
	"time"
)

// User contains information about a user such as their name, email,
// and information about when they are created and updated
type User struct {
	ID        int        `json:"id"`
	FirstName string     `json:"first_name"`
	LastName  string     `json:"last_name"`
	Email     string     `json:"email"`
	CreateAt  time.Time  `json:"create_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
}

// Validate checks to ensure that the required fields have non-zero values.
// This could contain more elaborate validation but for this example, just non-zero checks.
func (user *User) Validate() error {
	if user.FirstName == "" {
		return errors.New("first_name is a required field: cannot be empty")
	}
	if user.LastName == "" {
		return errors.New("last_name is a required field: cannot be empty")
	}
	if user.Email == "" {
		return errors.New("email is a required field: cannot be empty")
	}
	return nil
}

// UsersPage represents a page of users for a list request
type UsersPage struct {
	Users    []*User `json:"users"`
	Count    int     `json:"count"`
	NextPage *int    `json:"next_page,omitempty"`
}

// UserService defines a contract for the backend services to perform basic CRUD on a User.
type UserService interface {
	User(id int) (*User, error)
	Users(page, per int, sort, order string) (*UsersPage, error)
	CreateUser(user *User) error
	UpdateUser(user *User) error
	DeleteUser(id int) error
}
